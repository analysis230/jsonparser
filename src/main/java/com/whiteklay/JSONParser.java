package com.whiteklay;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.json.JSONObject;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Set;
import java.util.Properties;
import java.util.concurrent.Future;
import java.util.stream.Stream;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.cli.*;


public class JSONParser {
    public static void print(String x) {
        System.out.println(x);
    }

    public static void main(String args[]) {
        Options options = new Options();


        Option brokers = new Option("b", "brokers", true, "comma separated list of brokers");
        brokers.setRequired(true);
        options.addOption(brokers);

        Option topic = new Option("t", "topic", true, "name of the topic to write into");
        topic.setRequired(true);
        options.addOption(topic);

        Option inputFile = new Option("f", "filePath", true, "path to the file");
        inputFile.setRequired(true);
        options.addOption(inputFile);

        Option start = new Option("s", "startLine", true, "starting line in the file");
        start.setRequired(true);
        options.addOption(start);

        Option nLines = new Option("l", "noLines", true, "number of lines to read");
        nLines.setRequired(true);
        options.addOption(nLines);



        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
            return;
        }

        String brokerList = cmd.getOptionValue("brokers");
        String topicName = cmd.getOptionValue("topic");
        String FilePath = cmd.getOptionValue("filePath");
        int startLine = Integer.parseInt(cmd.getOptionValue("startLine"));
        int noLines = Integer.parseInt(cmd.getOptionValue("noLines"));


        try {
            Stream<String> stream = Files.lines(Paths.get(FilePath));

            // create instance for properties to access producer configs
            Properties props = new Properties();

            //Assign localhost id
            props.put("bootstrap.servers", brokerList);

            props.put("linger.ms", 100);
            props.put("key.serializer",
                    "org.apache.kafka.common.serialization.StringSerializer");

            props.put("value.serializer",
                    "org.apache.kafka.common.serialization.StringSerializer");

            Producer<String, String> producer = new KafkaProducer<String, String>(props);


            stream.skip(startLine).limit(noLines).forEach((String record) -> {


                JSONObject obj = new JSONObject(record);

                String msisdn = obj.getString("msisdn");
                obj.remove("msisdn");
                Set<String> itr = obj.keySet();

                final CountDownLatch countDownLatch = new CountDownLatch(itr.size());
                try {
                    itr.forEach((String x) ->
                    {
                        if (!(obj.isNull(x))) {
                            JSONObject separatedObj = new JSONObject();

                            separatedObj.put("phoneNumber", msisdn);
                            separatedObj.put("attribute_name", x);
                            separatedObj.put("attribute_value", obj.get(x));
                            //Assign topicName to string variable


                            producer.send(new ProducerRecord<String, String>(topicName, separatedObj.toString()), (metadata, exception) ->
                            {
                                if (metadata != null) {
                                    System.out.printf("sent record(value=%s) " + "meta(partition=%d, offset=%d)\n", record, metadata.partition(), metadata.offset());
                                } else {
                                    exception.printStackTrace();
                                }
                                countDownLatch.countDown();

                            });
                        } else {
                            System.out.println(obj);
                            countDownLatch.countDown();
                        }


                    });
                    countDownLatch.await(10,

                            java.util.concurrent.TimeUnit.SECONDS);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            });
            producer.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
