#JSON PARSER
Takes in a Json with msisdn and multiple attributes and spits then out int the format
```
{"msisdn":<value>,"attribute_name":<value>, "attribute_value":<value>}
```
The program takes these parameters, all of which are necessary:

```
 -b,--brokers <arg>     comma separated list of brokers
 -f,--filePath <arg>    path to the file
 -l,--noLines <arg>     number of lines to read
 -s,--startLine <arg>   starting line in the file
 -t,--topic <arg>       name of the topic to write into
```

##sample command

```
java -cp JsonDetailsParsing-1.0-SNAPSHOT.jar com.whiteklay.JSONParser -b localhost:9092 -t original -f /Users/vipulrajan/Downloads/test.txt -s 0 -l 20
```
